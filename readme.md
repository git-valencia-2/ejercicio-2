1. Clona este repositorio en tu máquina
2. Borra la carpeta .git y el archivo readme.md.
3. Inicializa un repositorio local
4. Añade al stage todos los archivos y crea un commit inicial con todos ellos.
5. Abre el archivo lampara/lampara.java y añade esta nueva propiedad a la clase. Guarda el archivo.

```java
    private boolean tieneBombilla;
```

6. Abre el archivo lavadora/lavadora.java y haz que el método lava() sea privado. Guarda el archivo.
7. Añade los cambios al stage y haz un commit con ellos.
8. Abre el archivo lavadora/lavadora.java y corrige la visibilidad del método lava(), hazlo público. Guarda el archivo.
9. Abre el archivo lampara/lampara.java y añade este nuevo método. Guarda el archivo.

```java
    public void switch() {
        estaEncendida = !estaEncendida;
    }
```
  
10. Añade únicamente la corrección del punto 8 al último commit que has hecho.
11. Crea un nuevo commit con el cambio del punto 9.
12. Comprueba que te ha quedado un log con tres commits.
13. Comprueba que en el penúltimo commit has corregido bien la visibilidad del método lava().
